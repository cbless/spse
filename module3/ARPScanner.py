#!/usr/bin/env python
'''
Created on 22.04.2013

@author: christoph

you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/ARPScanner.py?at=master

'''

import argparse
import netifaces 
from netaddr import *
import socket
from scapy.all import srp1, Ether, ARP, conf


def getAddresses(interface):
    """
    this function gets the public IP-Address and the MAC-Address of the interface.
    @return: tuple ( ip address, netmack, mac address with ':' , mac address without ':')
    """    
    iface = netifaces.ifaddresses(interface)
    addr_info = iface[socket.AF_INET][0]
    addr = addr_info['addr']                    # ip address of our interface
    netmask = addr_info['netmask']              # netmask of our interface
    packet_info = iface[socket.AF_PACKET][0]
    mac_origin = packet_info['addr']              # mac address of our interface
    mac = mac_origin.replace(":", "")             # replace the ":" in the mac addr
    return (addr, netmask, mac_origin, mac)



def scan_subnet(interface):
    """
    this function performs an ARP-Scan on the local subnet
    @param interface: name of the public interface 
    """
    (ip, netmask, mac_origin, mac) = getAddresses(args.interface)
    
    broadcast_mac = "ff:ff:ff:ff:ff:ff"
    network = IPNetwork(ip,netmask) # see netaddr module 
    net_list = list(network)        # list all ip addresses in the network
    
    result = []
    for h in net_list:
        ether = Ether(dst=broadcast_mac) # build the ethernet header
        arp = ARP(pdst=str(h), hwdst=broadcast_mac) # build an arp packet
        request = ether /arp             # build the request packet 
        response = srp1(request, timeout=0.4, verbose=0) # send request and receive response. 
        if response : # if we got an answer put it into the result list
            result.append((response.psrc, response.hwsrc))
    return result
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This is an ARP-Scanner")
    parser.add_argument("-i", "--interface", required=False, default="eth0", 
                        help="Interface")
    args = parser.parse_args()
    
    result = scan_subnet(args.interface)
    for ip, mac in result:
        print "IP: {0} MAC: {1}".format(ip, mac)