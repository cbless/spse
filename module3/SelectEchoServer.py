#!/usr/bin/env python
'''
Created on 05.04.2013

@author: christoph

you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/SelectEchoServer.py?at=master

This programm is a simple Echo-Server which uses select module to handle 
multiple clients.
'''
import argparse
import socket
import select

def createServerSocket(ip, port):
    """ This function creates a server socket. 
    
    This function creates a server socket, sets the reuse option and 
    binds the socket to the given ip and port.
    
    @param ip: IP-Address to listen on
    @param port: Port to bind on server socket 
    @return: server socket
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((ip,port))
    sock.listen(10)
    return sock

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a multiproces echo server")
    parser.add_argument("-p", "--port", required=False, default="1337", 
                        help="port for TCP-Server")
    parser.add_argument("-i", "--interface", required=False, default="127.0.0.1", 
                        help="Interface")
    args = parser.parse_args()
    
    server = createServerSocket(args.interface,int(args.port))
    socketlist = [server]
    while True:
        inlist, outlist, errlist = select.select(socketlist,[],[])
        for s in inlist: 
            if s == server:  
                client, addr = server.accept()  
                socketlist.append(client)       
            else:
                data = s.recv(2024)
                if data:
                    s.send(data)
                else:
                    s.close()
                    socketlist.remove(s)
        
        