#!/usr/bin/env python

'''
Created on 05.04.2013

@author: christoph


you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/MultiProcessEchoServer.py?at=master

This programm is a simple Echo-Server which uses the multiprocessing package to 
spawn new processes for each client.

'''
import argparse
import socket
from multiprocessing import Process, Lock

class EchoProcess(Process):
    """
    This Class handles a client-connection
    """
    def __init__(self, client, addr):
        Process.__init__(self)
        self.client = client
        self.addr = addr
        
    def run(self):
        """
        this method handle a client-connection, print received data to stdout on the
        server side and send the data back to the client
        """
        ip , port = self.addr
        while True:
            try:
                data = self.client.recv(1024)
                if not data:
                    return
                self.client.send(data)  # return data to client
                print "[{0}] client {1}:{2} wrote: {3}".format(self.name, ip, port, data)
            except:
                print "Error while receiving data"
                return
        
        
    
def createServerSocket(ip, port):
    """ This function creates a server socket. 
    
    This function creates a server socket, sets the reuse option and 
    binds the socket to the given ip and port.
    
    @param ip: IP-Address to listen on
    @param port: Port to bind on server socket 
    @return: server socket
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((ip,port))
    sock.listen(10)
    return sock

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a multiproces echo server")
    parser.add_argument("-p", "--port", required=False, default="1337", 
                        help="port for TCP-Server")
    parser.add_argument("-i", "--interface", required=False, default="127.0.0.1", 
                        help="Interface")
    args = parser.parse_args()
    
    server = createServerSocket(args.interface,int(args.port))
    while True:
        client, addr = server.accept()      # accept the client
        worker = EchoProcess(client, addr)  # spawn a new Process for client
        worker.start()                      # start the process
        