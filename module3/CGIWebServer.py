#!/usr/bin/env python
'''
Created on 06.04.2013

@author: christoph
'''

from BaseHTTPServer import HTTPServer
from CGIHTTPServer import CGIHTTPRequestHandler


server = HTTPServer(('',10000), CGIHTTPRequestHandler)
server.serve_forever()