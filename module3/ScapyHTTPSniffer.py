#!/usr/bin/env python
'''
Created on 22.04.2013

@author: christoph


you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/ScapyHTTPSniffer.py?at=master


this program is a packet sniffer for the HTTP which prints out the data 
in a GET/POST Request 

'''
import argparse
from scapy.all import *

def printHeader(header):
    """
    this function prints out the HTTP-Header fields
    @param header: list of HTTP-header fields 
    """
    print "---------start of HTTP-Header ---------"
    for h in header:
        print h
    print "---------end of HTTP-Header ---------"

def http_callback(pkt):
    """
    this function print out the data in GET/POST-Request and all
    HTTP-Header fields
    @param pkt: received packet  
    """
    if pkt.haslayer(Raw):
        raw = pkt.getlayer(Raw).load
        r = raw.split("\r\n")
        if r[0].startswith("GET"):
            print "\n{0}".format(pkt.summary())
            data = r[0].split()
            if (len(data) > 1):
                print "GET-Request data: {0}".format(data[1])
                printHeader(r[1:])
        elif r[0].startswith("POST"):
            print "\n{0}".format(pkt.summary())
            data = r[0].split()
            if (len(data) > 1):
                print "POST-Request data: {0}".format(data[1])
                printHeader(r[1:])
        elif r[0].startswith("HTTP"):
            print "\n{0}".format(pkt.summary())
            printHeader(r[1:len(r)-1])
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This is a simple HTTP-Sniffer")
    parser.add_argument("-i", "--interface", required=False, default="eth0", 
                        help="Interface from which to capture data")
    args = parser.parse_args()
    
    # create a sniffer for the HTTP protocol
    pkts = sniff(iface=args.interface, filter="tcp port 80", prn=http_callback)
    