#!/usr/bin/env python
'''
Created on 04.04.2013

@author: christoph

you can find this file on bitbucket:gi
https://bitbucket.org/chbb/spse/src/master/module3/EchoServer.py?at=master

This program is a simple echo server. 
'''
import argparse
from SocketServer import TCPServer, BaseRequestHandler

class EchoTCPServer (TCPServer):
    allow_reuse_address = True

class EchoHandler(BaseRequestHandler):
    """
    The RequestHandler class for the EchoServer
    
    It overrides the handle() method to send back received data to the client.
    """
    def handle(self):
        try:
            data = self.request.recv(1024).strip()
            # print received data on server side
            print "{0} wrote: {1}".format(self.client_address, data)
            self.request.sendall(data)  # return data to client
        except Exception:
            print "Error while receiving data"



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a simple echo server")
    parser.add_argument("-p", "--port", required=False, default="1337", 
                        help="port for TCP-Server")
    parser.add_argument("-i", "--interface", required=False, default="127.0.0.1", 
                        help="Interface")
    args = parser.parse_args()
    
    try:
        server = EchoTCPServer((args.interface,int(args.port)), EchoHandler)
        server.serve_forever()
    except(KeyboardInterrupt, SystemExit):
        print "it is time for me to die"
    finally:
        server.shutdown()
        