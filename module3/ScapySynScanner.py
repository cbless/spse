#!/usr/bin/env python
'''
Created on 25.04.2013

you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/ScapySynScanner.py?at=master

@author: christoph
'''
import argparse

from scapy.all import *

def parse_ports(ports):
    """
    this function builds a portlist form the given parameter 'ports'. The 
    parameter 'ports' can contains a list of comma separated ports. Each of 
    this ports can be a portrange with a startport and an endport (separated 
    with '-'). 
    """
    portlist = []
    for port in ports.split(','):
        if '-' in port:
            portrange = port.split('-')
            for i in range(int(portrange[0]),int(portrange[1])+1):
                # append all ports in the portrange to the new portlist
                portlist.append(i)
        else:
            # append single port to portlist
            portlist.append(int(port))
    return portlist

def synScan(target, portlist):
    for port in portlist:
        ip = IP(dst= target)
        tcp = TCP(sport=RandShort(),dport=port, flags="S")
        response = sr1(ip/tcp, timeout=0.5, verbose= False)
        if response and response.getlayer(TCP).flags == 0x12: # 0x12 = SYN-ACK
            print "{0} is open ".format(str(port))
        
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This is an ARP-Scanner")
    parser.add_argument("-p", "--port", required=False, default="1-1024", 
                        help="Port(s) to scan. Use ',' to separate ports \
                        and '-' for a range of ports")
    parser.add_argument("-t", "--target", required=True,  
                        help="target ip address")
    parser.add_argument("-v", "--verbose", required=False, default=False, 
                        action='store_true', help="verbose output")
    args = parser.parse_args()
    
    portlist = parse_ports(args.port)   # create a portlist form cli parameter -p or --port
    synScan(args.target, portlist)