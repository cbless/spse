#!/usr/bin/env python
'''
Created on 04.04.2013

@author: christoph

you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/ThreadedEchoServer.py?at=master

This program is a simple threaded echo server. 
'''
import argparse
from thread import allocate_lock
import threading
from SocketServer import TCPServer, BaseRequestHandler, ThreadingMixIn

global lock

class ThreadedEchoTCPServer (ThreadingMixIn, TCPServer):
    allow_reuse_address = True

class ThreadedEchoHandler(BaseRequestHandler):
    """
    The RequestHandler class for the ThreadedEchoServer
    
    It overrides the handle() method to send back received data to the client.
    """
    def handle(self):
        while True:
            try:
                data = self.request.recv(1024)
                if not data:
                    return
                self.request.sendall(data)  # return data to client
                t = threading.current_thread()
                lock.acquire()
                # print received data on server side
                print "[Thread:{0}] client {1} wrote: {2}".format(t, self.client_address, data.strip())
                lock.release()
            except Exception:
                print "Error while receiving data"
                return



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a threaded echo server")
    parser.add_argument("-p", "--port", required=False, default="1337", 
                        help="port for TCP-Server")
    parser.add_argument("-i", "--interface", required=False, default="127.0.0.1", 
                        help="Interface")
    args = parser.parse_args()
    
    try:
        lock = threading.Lock()
        server = ThreadedEchoTCPServer((args.interface,int(args.port)), 
                                       ThreadedEchoHandler)
        server.serve_forever()
    except(KeyboardInterrupt, SystemExit):
        print "it is time for me to die"
    finally:
        server.shutdown()
        