#/usr/bin/env python
'''
Created on 15.04.2013

@author: christoph

you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/ArpRequest.py?at=master


Module 3 Part 5:

This program sends ARP-Requests using raw sockets. 


this script requires the 'netifaces' module. you can install it with 'pip'
    pip install netifaces     (this module needs python-dev)
    
    to install python-dev on a debian based linux use:
    'apt-get install python-dev' or 'aptitude install python-dev'
'''

import socket 
import struct
import netifaces
import argparse         # requirse python 2.7
import sys
import binascii

# for more details see http://www.networksorcery.com/enp/protocol/arp.htm
HARDWARE_TYPE = "\x00\x01"        # Hardwaretype for Ethernet
ARP_REQUEST = "\x00\x01"          # Opcode for ARP Request
PROTO_TYPE = "\x08\x00"           # Protocol type = IP

def createSocket():
    """
    this function creates a raw socket
    @return: new socket
    """
    try:
        s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.htons(0x806))
        s.bind(("eth0", socket.htons(0x806)))
    except IOError:
        print "can not create socket (you have to be root)"
        sys.exit(-1)
    return s

def buildEtherheader(smac):
    """
    this function builds an ethernet header
    @param smac: source mac address
    @return packed ethernet header with ethertype=ARP   

    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 
    ------------------------------
    dest. mac  | source mac | type
    ------------------------------
    
    destination mac : 6 bytes
    source mac : 6 bytes
    ether type : 2 bytes
    """
    ether = struct.pack("!6s6s2s", 
                        "\xFF\xFF\xFF\xFF\xFF\xFF", # Broadcast address
                        smac.decode('hex') ,        # source mac
                        "\x08\x06")                 # ether type = ARP
    return ether
    
def buildArpRequest(smac, src_ip, dst_ip):
    """
    this function builds the ARP-Request
    @param smac: source mac address
    @param src_ip: source ip
    @param dst_ip: destination/target ip
    @return packed ARP Header 
    
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    ---------------------------------------------------------------
    Hardware type                  |    Protocol type
    ---------------------------------------------------------------
    HW length      | Proto length  |   Opcode
    ---------------------------------------------------------------
                      source hardware address
    ---------------------------------------------------------------
                      source protocol address
    ---------------------------------------------------------------
                    destination hardware address
    ---------------------------------------------------------------
                    destination protocol address
    ---------------------------------------------------------------
                                data
    """
    arp = struct.pack("!2s2s1s1s2s", 
                      HARDWARE_TYPE, 
                      PROTO_TYPE, 
                      "\x06",                       # Hardware address length
                      "\x04",                       # Protocol address length
                      ARP_REQUEST                   # Opcode
                      )
    arp += struct.pack("!6s", smac.decode('hex'))           # source mac
    arp += struct.pack("!4s", socket.inet_aton(src_ip))     # source ip
    arp += struct.pack("!6s", "\x00\x00\x00\x00\x00\x00")   # destination mac
    arp += struct.pack("!4s", socket.inet_aton(dst_ip))     # destionation ip
                      
    return arp


    
def getAddresses():
    """
    this function gets the public IP-Address and the MAC-Address of the interface.
    @return: tuple ( ip address, mac address with ':' , mac address without ':')
    """    
    iface = netifaces.ifaddresses(args.interface)
    iface_ip = iface[2][0]['addr']              # ip address of our interface
    iface_mac_addr = iface[17][0]['addr']       # mac address of our interface
    iface_mac = iface_mac_addr.replace(":", "") # replace the ':' in the mac address
    return (iface_ip, iface_mac_addr, iface_mac)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This is a simple tool which \
        sends ARP-Requests using raw sockets")
    parser.add_argument("-i", "--interface", required=False, default="eth0", 
                        help="Interface")
    parser.add_argument("-t", "--target", required=True, help="IP for our request")
    args = parser.parse_args()
    
    # create a raw socket and bind it to an interface
    s = createSocket()
    # get the own ip and mac address
    (ip, mac_formatted, mac) = getAddresses()
    # build the ethernet header
    ether = buildEtherheader(mac) 
    # build the data for ARP-Request
    arp_data = buildArpRequest(mac, ip, args.target)
    
    try:
        while len(arp_data) < 46:  # ether + arp must have 60 bytes
            arp_data += struct.pack("B", 0x00)
        s.send( ether + arp_data)
        s.settimeout(1)
        data, addr = s.recvfrom(2048)
        resp_mac = binascii.hexlify(data[6:12])
        print "Response from : {0}".format(resp_mac)
    except socket.timeout:
        print "no response"
        sys.exit(-1)
        
    
        
