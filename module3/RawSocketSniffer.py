#/usr/bin/env python
'''
Created on 08.04.2013

@author: christoph

you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/RawSocketSniffer.py?at=master


this program is a simple packet sniffer which uses raw sockets.
'''
import argparse
import socket
import fcntl
import sys
import ctypes
import struct
import binascii

class ifreq(ctypes.Structure):
    """
    Interface request Structure used for socket ioctl's. 
    @see /usr/include/net/if.h
    """
    ## size form interface name
    IF_NAMESIZE = 16
    
    _fields_ = [
               ## Interface name
               ("ifr_ifrn", ctypes.c_char * IF_NAMESIZE),
               ## Flags
               ("ifr_flags", ctypes.c_short)
    ]  
  
## Receive all packets
# @see /usr/include/net/if.h
IFF_PROMISC = 0x100     
## Socket configuration controls 
# get flags 
# @see /usr/include/linux/sockios.h
SIOCGIFFLAGS = 0x8913
## Socket configuration controls 
# set flags 
# @see /usr/include/linux/sockios.h
SIOCSIFFLAGS = 0x8914
        
def createSocket():
    """
    this function creates a raw socket
    @return: new socket
    """
    try:
        s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.htons(0x800))
        #s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    except IOError:
        print "can not create socket (you have to be root)"
        sys.exit(-1)
    return s
        
def enablePromisc(sock, ifr):
    """
    this function enables the promiscuous mode.
    @param sock: raw socket
    @param ifr: instance of the ifreq class   
    """
    try:
        fcntl.ioctl(sock.fileno(), SIOCGIFFLAGS, ifr)
        ifr.ifr_flags |= IFF_PROMISC
        fcntl.ioctl(sock.fileno(), SIOCSIFFLAGS, ifr)
    except IOError:
        print "can not enable promiciuous mode"
        sock.close()
        sys.exit(-2)

def disablePromisc(sock, ifr):
    """
    this function disables the promiscuous mode
    @param sock: raw socket
    @param ifr: instance of the ifreq class 
    """
    ifr.ifr_flags &= ~IFF_PROMISC
    fcntl.ioctl(sock.fileno(), SIOCSIFFLAGS, ifr) 
    
def formatMac(origin):
    mac = ":".join(["%s%s" % (origin[i], origin[i+1]) for i in range(0,12,2)])
    return mac
    
def parseEther(ether_header):
    # unpack the ethernet header
    # 6 bytes : destination MAC
    # 6 bytes : source MAC
    # 2 bytes : ether type
    eth_hdr = struct.unpack("!6s6s2s", ether_header[0:14])
    dst = binascii.hexlify(eth_hdr[0])      # destination MAC address
    src = binascii.hexlify(eth_hdr[1])      # source Mac address
    eth_type = binascii.hexlify(eth_hdr[2]) # Ether Type (0800 => IP)
    return (dst, src, eth_type)

def printEther(ether_tuple):
    (dst, src, eth_type) = ether_tuple
    print "Ethernet (dst={0}, src={1}, type={2})".format(formatMac(dst), formatMac(src), eth_type)
    
def parseIP(ip_header):
    """
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |Version|  IHL  |Type of Service|          Total Length         |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |         Identification        |Flags|      Fragment Offset    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  Time to Live |    Protocol   |         Header Checksum       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                       Source Address                          |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Destination Address                        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Options                    |    Padding    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    """
    iph = struct.unpack("!BBHHHBBH4s4s", ip_header)
    ver_ihl = iph[0]
    version = ver_ihl >> 4
    ihl = ver_ihl & 0xF
    ip_len = ihl*4
    ip_tos = iph[1]
    ip_id = iph[3] 
    ttl = iph[5]
    proto = iph[6]
    src = socket.inet_ntoa(iph[8])
    dst = socket.inet_ntoa(iph[9])
    return (version, ihl, ip_len, ip_tos, ip_id, ttl, proto, src, dst)

def printIP(ip_tuple):
    (version, ihl, ip_len, ip_tos, ip_id, ttl, proto, src, dst) = ip_tuple
    output = "IP (version={0}, length={1}, ttl={2}, proto={3}, src={4}, dst={5})"
    print output.format(str(version), str(ip_len), str(ttl), str(proto), src,dst)
    
def parseTCP(tcp_header):
    """
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |          Source Port          |       Destination Port        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                        Sequence Number                        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Acknowledgment Number                      |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |  Data |           |U|A|P|R|S|F|                               |
    | Offset| Reserved  |R|C|S|S|Y|I|            Window             |
    |       |           |G|K|H|T|N|N|                               |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |           Checksum            |         Urgent Pointer        |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                    Options                    |    Padding    |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                             data                              |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    """
    tcph = struct.unpack("!HHLLHHHH", tcp_header)
    sport = tcph[0]             # source port 
    dport = tcph[1]             # destination port
    seq = tcph[2]               # sequence number
    ack_num = tcph[3]           # acknowledgment number
    doff_flags = tcph[4]        # data offset | reserved | flags
    win = tcph[5]               # window size
    chksum = tcph[6]            # checksum
    urg_pointer = tcph[7]       # urgent pointer
    
    tcp_tuple = (sport, dport, seq, ack_num, doff_flags, win, chksum, urg_pointer)
    return tcp_tuple

def printTCP(tcp_tuple):
    (sport, dport, seq, ack_num, doff_flags, win, chksum, urg_pointer) = tcp_tuple
    tcp_output = "TCP( sport={0}, dport={1}, seq={2}, ack_num={3}, doff_flas={4}, win={5}, chksum={6}, urg_pointer={7})"
    print tcp_output.format(str(sport), str(dport), str(seq), str(ack_num), str(doff_flags), str(win), str(chksum), str(urg_pointer))  

def parseUDP(udp_header):
    """
     0      7 8     15 16    23 24       31
     +--------+--------+--------+--------+
     |     Source      |   Destination   |
     |      Port       |      Port       |
     +--------+--------+--------+--------+
     |                 |                 |
     |     Length      |    Checksum     |
     +--------+--------+--------+--------+
    """
    udp = struct.unpack("!HHHH", udp_header)
    sport = udp[0]
    dport = udp[1]
    udplen = udp[2]
    checksum = udp[3]
    return (sport, dport, udplen, checksum)
    
def printUDP(udp_tuple):
    (sport, dport, udplen, chksum) = udp_tuple
    output = "UDP(sport={0}, dport={1}, len={2}, chksum={3}"
    print output.format(str(sport), str(dport), str(udplen), str(chksum))
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This is a simple packet snifer \
        which uses raw sockets")
    parser.add_argument("-i", "--interface", required=False, default="eth0", 
                        help="Interface from which to capture data")
    args = parser.parse_args()
    
    ifr = ifreq()
    ifr.ifr_ifrn = args.interface
    
    # create a raw socket and enable promiscuous mode
    s = createSocket()
    enablePromisc(s, ifr)
         
    try:
        while True:
            data, addr = s.recvfrom(2048)
            if data:
                print "------------------------------------------"
                ether_tuple = parseEther(data[0:14])
                (d_mac, s_mac, eth_type) = ether_tuple
                printEther(ether_tuple)
                if eth_type == '0800':
                    iph = parseIP(data[14:34])
                    printIP(iph) 
                    # split the IP header 
                    (ip_ver, ihl, ip_len, ip_tos, ip_id, ttl, proto, src, dst) = iph
                    if proto == 6:
                        tcp_start = 14+ip_len       # start of tcp header
                        tcp_end = tcp_start + 20    # end of tcp header
                        tcph = parseTCP(data[tcp_start:tcp_end])
                        printTCP(tcph)
                        #print "data: {0}".format(data[tcp_end:])
                    elif proto == 17:
                        udp_start = 14+ ip_len      # start of udp header
                        udp_end = udp_start + 8     # end of upd header
                        udp = parseUDP(data[udp_start:udp_end])
                        printUDP(udp)
                print "------------------------------------------"
    except (KeyboardInterrupt, SystemExit):
        print "Error occured"
        pass
    finally:
        # disable promiscuous mode
        disablePromisc(s, ifr)
        s.close()
        
    
    