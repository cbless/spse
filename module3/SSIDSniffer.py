#!/usr/bin/env python
'''
Created on 22.04.2013

@author: christoph


you can find this file on bitbucket:
https://bitbucket.org/chbb/spse/src/master/module3/SSIDSniffer.py?at=master



'''
import argparse
from scapy.all import *

# stores all known SSIDS
ssids = [] 

def wifi_callback(pkt):
    """
    this function prints out all new SSIDs
    """
    # type '0' = Management Frame
    # subtype '8' = Beacon Frame
    if pkt.haslayer(Dot11) and pkt.type == 0 and pkt.subtype == 8:
        # new SSID?
        if not pkt.info in ssids:
            ssids.append(pkt.info)
            print "MAC: {0} SSID: {1}".format(pkt.addr2, pkt.info)
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This is a SSID sniffer")
    parser.add_argument("-i", "--interface", required=False, default="wlan0", 
                        help="Interface from which to capture data")
    args = parser.parse_args()
    
    pkts = sniff(iface=args.interface, prn=wifi_callback)
    