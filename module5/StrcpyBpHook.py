"""
created on 24.09.2013

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module5/StrcpyBpHook.py?at=master

This PyCommand installs a hook for the strcpy function which prints the 
parameter of strcpy to the log window.
"""
import immlib

imm = immlib.Debugger()


def toHex(addr):
	"""
	converts an integer address to hex
	"""
	return "%08X" % addr
	
class StrcpyHook(immlib.BpHook):
	
	def __init__(self):
		immlib.BpHook.__init__(self)
		func = "strcpy"
		addr = imm.getAddress(func)	# get the address of the strcpy function
		self.add(func, addr)		# install the hook 
		imm.log("[+] Function {0} at address 0x{1} has been hooked".format(func,toHex(addr)))
		
	def run(self, regs):
		msg = "[+] Strcpy called:\n"
		msg += "[+] Addresses: EIP: 0x{0}, ARG1: 0x{1}, ARG2:0x{2}\n"
		msg += "[+] length of Arg2: {3}, value of Arg2: {4}"
		
		eip = imm.readLong(regs['ESP']) 	# get the EIP on stack
		arg1 = imm.readLong(regs['ESP'] + 4)# get the address of the first argument
		arg2 = imm.readLong(regs['ESP'] + 8)# get the address of the second argument
		stringArg2= imm.readString(arg2) 	# read the value of the second argument
		length = len(stringArg2) 
		
		msg = msg.format(toHex(eip), toHex(arg1), toHex(arg2), length, stringArg2)
		imm.logLines(msg,highlight=True)
		
def main(args):
	hook = StrcpyHook()
	return "[+] Hook for Strcpy is installed"
	