"""
created on 23.09.2013

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module5/findmodules.py?at=master

This PyCommand dumps some infos about all loaded modules.
"""

import immlib

def toHex(addr):
	"""
	converts an integer address to hex
	"""
	return "%08X" % addr

def main(args):
	imm = immlib.Debugger()
	
	# create a table for our output
	table = imm.createTable("Module Information", ['Name', 'Base address', 'Size', 'Entry Point', 'Version'])
		
	modules = imm.getAllModules()
	
	for m in modules.values():
		# dump some infos about the modules and add them to the table
		name = m.getName()
		mbase = toHex(m.getBaseAddress())
		size = toHex(m.getSize())
		entryPoint = toHex(m.getEntry())
		version = m.getVersion()
		
		table.add(0, [name, mbase, size, entryPoint, version])
		
	return "ready"