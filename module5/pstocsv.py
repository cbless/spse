"""
created on 23.09.2013

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module5/pstocsv.py?at=master

This PyCommand dumps a list of all running processes to a csv file.
"""


from immlib import *

def main(args):
	imm = Debugger()
	ps = imm.ps()
	with open('ps.csv', 'w') as f:
		f.write("pid;name;pathi;service;tcp list;udp list\n")
		for p in ps:
			pstr = "{0};{1};{2};{3};{4};{5}\n".format(p[0],p[1],p[2],p[3],p[4],p[5])
			f.write(pstr)
	
	return "list of processes writen to file"
