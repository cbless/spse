
import immlib
from struct import unpack
imm = immlib.Debugger()


def toHex(addr):
    """
    converts an integer address to hex
    """
    return "%08X" % addr

class BindBpHook(immlib.BpHook):

    def __init__(self):
        immlib.BpHook.__init__(self)
        func = "ws2_32.bind"
        addr = imm.getAddress(func)	# get the address of the strcpy function
        self.add(func, addr)		# install the hook
        imm.log("[+] Function {0} at address 0x{1} has been hooked".format(func,toHex(addr)))

    def run(self, regs):
        msg = "[+] Bind called:\n"
        addr = unpack("L", imm.readMemory(regs['ESP'] + 0x8, 4))[0]
        sockaddr = unpack("BBBBBBBB",imm.readMemory(addr, 8))
        # sockaddr.sin_family
        print "sockaddr.sin_family: %d" % sockaddr[0]
        # sockaddr.sin_port
        print "sockaddr.sin_port: %d" % (sockaddr[2] * 256 + sockaddr[3])
        # sockaddr.sin_addr
        print "sockaddr.sin_addr: %d.%d.%d.%d" % (sockaddr[4],sockaddr[5],sockaddr[6],sockaddr[7])

class SendBpHook(immlib.BpHook):


    def __init__(self):
        immlib.BpHook.__init__(self)
        func = "ws2_32.send"
        addr = imm.getAddress(func)	# get the address of the strcpy function
        self.add(func, addr)		# install the hook
        imm.log("[+] Function {0} at address 0x{1} has been hooked".format(func,toHex(addr)))

    def run(self, regs):
        msg = "[+] Send called:\nargs (len: {0} buff_addr: {1} buffer: {2})"
        # .. we read the socket (1. arg),
        # buffer address (2. arg)
        # and length (3. arg) from the stack
        sock = unpack("I", imm.readMemory(regs['ESP'] + 0x04, 4))[0]
        buffer_addr = unpack("L", imm.readMemory(regs['ESP'] + 0x08, 4))[0]
        length = unpack("L", imm.readMemory(regs['ESP'] + 0x0C, 4)) [0]

        # read buffer from the buffer_addr
        buf = imm.readMemory(buffer_addr, length)
        msg = msg.format(length, buffer_addr, buf)
        imm.logLines(msg, Highlight=True)


def main(args):
    hook = BindBpHook()
    s_hook = SendBpHook()
    return "[+] Hooks for Bind and Send are installed"