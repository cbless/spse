"""
created on 23.09.2013

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module5/findaddresses.py?at=master

This PyCommand searches for instructions given as argument. For every hit it 
will display the module name, the base address of the module, the address of the instruction, 
the instruction, and the 'Access Flag' of the memory page
"""
import immlib

def toHex(addr):
	"""
	converts an integer address to hex
	"""
	return "%08X" % addr

def main(args):
	imm = immlib.Debugger()
	
	instructions = " ".join(args)
	
	if len(instructions) == 0:
		return "[-] missing instruction"
	
	results = imm.search(imm.assemble(instructions))
	
	# create a table for our output
	table = imm.createTable("Instruction Locations", ['Module', 'Base address', 'Instruction Address', 'Instruction', 'Code Page Access'])
	
	for r in results:
		mod = imm.findModule(r)
		if not mod:
			table.add(0, ["", "",toHex(r), instructions, ""])
			continue
		else:
			m = imm.getModule(mod[0])
			page = imm.getMemoryPageByAddress(r)
			access = page.getAccess(human=True)
			table.add(0, [m.getName(), toHex(m.getBaseAddress()),toHex(r), instructions, access])
			

	return "[*] ready "