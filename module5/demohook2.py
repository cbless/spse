"""
created on 23.09.2013

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module5/demohook2.py?at=master


"""
import immlib

def toHexStr(addr):
	"""
	converts an integer address to hex
	"""
	return "0x%08X" % addr
	
class DemoHook2(immlib.AccessViolationHook):
	def __init__(self):
		immlib.AccessViolationHook.__init__(self)
		
	def run(self,regs):
		imm = immlib.Debugger()
		msg = "[+] AccessViolation detected!\n"
		msg += "[+] EIP: {0}, ESP: {1}\n"
		
		eip = regs['EIP']
		esp = regs['ESP']
		
		buf = imm.readString(esp)
		
		if len(buf):
			msg += "[+] String length at ESP: {2} String: {3}"
			msg = msg.format(toHexStr(eip),toHexStr(esp), len(buf), buf)
			imm.logLines(msg, highlight=True)
		else:
			msg = msg.format(toHexStr(eip),toHexStr(esp) )
			imm.logLines(msg, highlight=True)
		
def main (args):
	hook = DemoHook2()
	hook.add("Demo Hook")
	return "[+] demohook2 installed"
