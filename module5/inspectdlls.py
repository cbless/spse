"""
created on 24.09.2013

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module5/inspectdlls.py?at=master

"""

import immlib
import struct

# The DLL can be relocated at load time (ASLR)
IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE = 0x0040 
# The image is compatible with data execution prevention (DEP)
IMAGE_DLLCHARACTERISTICS_NX_COMPAT = 0x0100	
# The image does not use structured exception handling (SEH)
IMAGE_DLLCHARACTERISTICS_NO_SEH = 0x0400
	
def toHex(addr):
	"""
	converts an integer address to hex
	"""
	return "%08X" % addr
	
def toHexStr(addr):
	"""
	converts an integer address to a hex string with a leading 0x
	"""
	return "0x%08X" % addr

def inspectModule(imm, m):
	msg = "\n[+] Inspecting module: {0}\n"
	msg += "[+] version: {4}\n"
	msg += "[+] base address: {1}, size: {2}, entry point: {3}\n"
	
	name = m.getName()
	base_addr = m.getBaseAddress()
	mbase = toHexStr(base_addr)
	size = toHexStr(m.getSize())
	entryPoint = toHexStr(m.getEntry())
	version = m.getVersion()
	
	msg = msg.format(name, mbase, size, entryPoint, version)
	imm.logLines(msg)
	
	dep = True
	aslr = True
	peoffset = struct.unpack('<L', imm.readMemory(base_addr + 0x3c,4))[0]
	pebase = base_addr + peoffset 
	flags = struct.unpack('<H', imm.readMemory(pebase + 0x5e,2))[0]
	if (flags & IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE) == 0:
		aslr = False
	if (flags & IMAGE_DLLCHARACTERISTICS_NX_COMPAT) ==0:
		dep = False
	protection_str = "dep = {0}, aslr = {1}".format(str(dep), str(aslr))
	if aslr or dep:
		imm.log("[-] " + protection_str)
	else:
		imm.log("[+] " + protection_str, highlight=True)
	functions = imm.getAllFunctions(base_addr)
	
	msg += "[+] {0} functions found in Module:\n".format(len(functions))
	for f in functions:
		func_str = "[+] Function: {0}, start address: {1}\n"
		func = imm.getFunction(f)
		func_str = func_str.format(func.getName(), func.getStart())
		msg += func_str
		
	imm.logLines(msg)

def main(args):
	imm = immlib.Debugger()
	
	# create a table for our output
	# table = imm.createTable("Module Information", ['Name', 'Base address', 'Size', 'Entry Point', 'Version'])
		
	modules = imm.getAllModules()
	for m in modules.values():
		inspectModule(imm, m)
	
	return "ready"
