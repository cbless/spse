#/usr/bin/env python
'''
Created on 27.10.2013

@author: Christoph Bless
'''

import argparse
import csv
import itertools
import mechanize 
import random
import re
from collections import Counter

class WebBrowser(mechanize.Browser):
    """
    this class extends the mechanize Browser class and adds some functions
    to change the useragent.
    """
    def __init__(self, useragents = [], redirects=True, gzip=False, 
                 equiv=True, robots=False, referer=True):
        self.default_useragents =[
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:23.0) Gecko/20131011 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36',
            'Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0'
            ]
        mechanize.Browser.__init__(self)
        self.set_handle_redirect(redirects)
        self.set_handle_gzip(gzip)
        self.set_handle_equiv(equiv)
        self.set_handle_robots(robots) 
        self.set_handle_referer(referer)
        self.set_handle_refresh(mechanize._http.HTTPRefererProcessor(), max_time = 1)
        if len(useragents) == 0:
            useragents.extend(self.default_useragents)
        self.useragents = useragents
        cj = mechanize.CookieJar()
        self.cookie_jar = cj
        self.set_cookiejar(cj)
        self.changeUseragent()  # set a random useragent string in the HTTP Header 
    
    def printInfo(self, key, value=None):
        if value ==None:
            print "[+] {0}".format(key)
        else:
            print "[+] {0}: {1}".format(key, value)
        
    def displayCookies(self):
        self.printInfo("current cookies")
        for cookie in self.getCookies():
            self.printInfo(str(cookie))
            
    def getCookies(self):
        return self.cookie_jar
        
    def addUseragent(self, useragent):
        """
        add a new useragent to the useragent-list
        """
        self.useragents.append(useragent)
    
    def removeUseragent(self, useragent):
        """
        remove the useragent from the useragent list
        """
        self.useragents.remove(useragent)
    
    def changeUseragent(self, useragent=None):
        """
        change the useragent string for the next requests.
        
        @param useragent: useragent string which will be used, if useragent 
            is not set a random useragent will be used
        """
        if useragent == None:
            useragent = random.randint(0, len(self.useragents)-1)
        self.addheaders = [('User-Agent', useragent)]

    def getRobots(self, url):
        """
        Reads the robots.txt from the given URL and return its content.
        """
        if not url.startswith("http://") or url.startswith("https://"):
            url = "http://"+url
        if not url.endswith("/"):
            url = url + "/"
        
        self.open(url + "robots.txt")
        robots = self.response().read()
        return robots
    
def findDisallowed(robots):
    """
    Find all directories in the robots.txt which are disallowed.
    @param robots: content of the robots.txt 
    @return: list with all disallowed directories
    """
    dirs = re.findall(r"Disallow:\s*(\/\w*\/)",robots)
    return dirs
       
       
def main(args):
    br = WebBrowser()
    cnt = Counter()
    with open(args.file, "r") as fh:
        reader = csv.reader(fh)
        for row in itertools.islice(reader,args.n):
            if len(row) == 2: 
                try:
                    robots = br.getRobots(row[1])
                    disallowed =  findDisallowed(robots)
                    for d in disallowed:
                        cnt[d] += 1
                    print "[+] site {0} fetched successful".format(row[1])
                except:
                    print "[-] error on fetching {0}".format(row[1])
                    pass
    print cnt.most_common(40)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-f", "--file", required=True,
                        help="csv with top 1000 sites from alexa.com")
    parser.add_argument("-n", required=False, default=1000, type=int,
                        help="Number of domains to fetch")
    args = parser.parse_args()
    
    main(args)
    
    