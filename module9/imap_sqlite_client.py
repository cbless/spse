#/usr/bin/env python
'''
Created on 27.10.2013

@author: christoph
'''
import argparse
import email
from imapclient import IMAPClient
import getpass
import email
import sqlite3

folders = [
    'INBOX',
    #'[Gmail]/Drafts',
    #'[Gmail]/Important',
    #'[Gmail]/Sent Mail',
    #'[Gmail]/Spam',
    #'[Gmail]/Starred',
]

class DB:

    def __init__(self, dbname):
        self.dbname = dbname


    def create(self):
        with sqlite3.connect(self.dbname) as con:
            cur = con.cursor()
            cur.execute("CREATE TABLE mails(mail_to TEXT, mail_from TEXT, mail_header TEXT, msg TEXT)")


    def insertAll(self, mails):
        with sqlite3.connect(self.dbname) as con:
            for mail in mails:
                mail_to, mail_from, mail_header, mail_content = mail
                header_str = " ".join(mail_header)
                con.cursor().execute("INSERT INTO mails VALUES(?,?,?,?)", (mail_to, mail_from, header_str, mail_content))

    def insert(self, mail_to, mail_from, mail_header, mail_content):
        with sqlite3.connect(self.dbname) as con:
            header_str = " ".join(mail_header)
            con.cursor().execute("INSERT INTO mails VALUES(?,?,?,?)", (mail_to, mail_from, header_str, mail_content))


def connect(server_name, ssl):
    user = getpass.getpass("username: ")
    pw = getpass.getpass("password: ")
    client = IMAPClient(server_name, use_uid=True, ssl=ssl)
    client.login(user, pw)
    return client

def parse_email(mail):
    msg = email.message_from_string(mail)
    mail_to = msg['To']
    mail_from = msg['From']
    mail_header = msg.items()
    mail_content = ""
    maintype = msg.get_content_maintype()
    if maintype == 'multipart':
        for part in msg.get_payload():
            if part.get_content_type() == 'text':
                mail_content= msg.get_payload()
    elif maintype == 'text':
        mail_content= msg.get_payload()
    return (mail_to, mail_from, mail_header, mail_content)


def main(args):
    print args
    client = connect(args.server, args.ssl)
    db = DB('mail.db')
    db.create()
    result = []
    for f in folders:
        folder = client.select_folder(f, readonly=True)
        print '%d messages in INBOX' % folder['EXISTS']
        messages = client.search(['NOT DELETED'])
        print "%d messages in folder %s" % (len(messages), folder)
        print "Messages:"
        response = client.fetch(messages, ['BODY.PEEK[]'])
        client.close_folder()
        for msgid, data in response.iteritems():
            msg_string = data['BODY[]']
            result.append(parse_email(msg_string))
    client.logout()
    db.insertAll(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-s", "--server", required=False, default="imap.gmail.com",
                        help="server to connect")
    parser.add_argument("--ssl", required=False, action="store_true",
                        help="don't use ssl")
    args = parser.parse_args()
    
    main(args)
