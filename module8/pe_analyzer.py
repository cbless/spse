#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 18.10.2013

@author: Christoph Bless
'''
import argparse
import pefile
import peutils
import hashlib
import re

LINE = "-" * 80

        
class PeAnalyzer():
    
    def __init__(self, filename):
        self.pe = pefile.PE(filename, fast_load=True)
        self.filename = filename
        self.pe.parse_data_directories(directories=[
                pefile.DIRECTORY_ENTRY['IMAGE_DIRECTORY_ENTRY_IMPORT'],             
                pefile.DIRECTORY_ENTRY['IMAGE_DIRECTORY_ENTRY_EXPORT'],
                pefile.DIRECTORY_ENTRY['IMAGE_DIRECTORY_ENTRY_RESOURCE'],
                pefile.DIRECTORY_ENTRY['IMAGE_DIRECTORY_ENTRY_DEBUG'],
                pefile.DIRECTORY_ENTRY['IMAGE_DIRECTORY_ENTRY_TLS']                               
                ])
        try:
            with open(self.filename, "rb") as f:
                self.data = f.read()
        except:
            pass
        
    
    def isPacked(self):
        """ 
        checks if the PE is packed
        
        @rtype Boolean
        @return True if PE is packed, false otherwise 
        """
        return (peutils.is_probably_packed(self.pe))
    
    def getPeType(self):
        if self.pe.is_dll():
            return "dll"
        elif self.pe.is_exe():
            return "executable"
        elif self.pe.is_driver():
            return "driver"
            
    def fileInfo(self):
        """
        This function collects some basic information about the PE file.
        
        @rtype list
        @return list of printable lines
        """
        result = []
        result.append(self.createHeader("File Info"))
        fmt = "%-20s: %s"
        
        result.append(fmt % ("Filename", self.filename))
        result.append(fmt % ("PE-Type", self.getPeType()))
        result.append(fmt % ("Packed", str(self.isPacked())))
        result.append(fmt % ("Suspicous", str(peutils.is_suspicious(self.pe))))
        
        if self.pe.verify_checksum():
            checksum_valid = "valid"
        else:
            checksum_valid = "invalid"
        result.append(fmt % ("Checksum" , checksum_valid))
        
            
        if self.data:
            result.append(fmt % ("Size", "%d bytes" % len(self.data) ))
            result.append(fmt % ("MD5", hashlib.md5(self.data).hexdigest()))
            result.append(fmt % ("sha1", hashlib.sha1(self.data).hexdigest()))
            result.append(fmt % ("sha256", hashlib.sha256(self.data).hexdigest()))
            result.append(fmt % ("sha512", hashlib.sha512(self.data).hexdigest()))    
        
        result.append("")
        
        if hasattr(self.pe, "VS_VERSIONINFO"):
            for fileinfo in self.pe.FileInfo:
                if fileinfo.Key == 'StringFileInfo':
                    for st in fileinfo.StringTable:
                        for entry in st.entries.items():
                            result.append( fmt % (entry[0], entry[1]))
                result.append("")
                if fileinfo.Key == 'VarFileInfo':
                    for var in fileinfo.Var:
                        result.append( fmt % var.entry.items()[0])
        return result
        
            
            
    def analyzeImports(self):
        """
        This function collects a list of libraries and functions which are used
        by the PE file.
        
        @rtype list
        @return list of printable lines
        """
        result = []
        result.append(self.createHeader("imported symbols"))
        if not hasattr(self.pe, "DIRECTORY_ENTRY_IMPORT"):
            return result
        for entry in self.pe.DIRECTORY_ENTRY_IMPORT:
            result.append(entry.dll)
            for imp in entry.imports:
                result.append("\t0x%08X\t%s"%(imp.address , imp.name))
        return result
    

    def analyzeExports(self):
        """
        This function collects a list of exported functions.
        
        @rtype list
        @return list of printable lines
        """
        result = []
        result.append(self.createHeader("exported symbols"))
        result.append("")
        fmt = "%-12s%-12s%-45s%s"
        result.append(fmt % ("Address", "Ordinal", "Name", "Forwarder" ))
        result.append(LINE)
        fmt = "0x%08X\t%i\t%-45s\t%s"
        if not hasattr(self.pe, "DIRECTORY_ENTRY_EXPORT"):
            return result
        for exp in self.pe.DIRECTORY_ENTRY_EXPORT.symbols:
            if exp.name is not None:
                if exp.forwarder is None:
                    result.append(fmt % (exp.address, exp.ordinal, exp.name, exp.forwarder))
                else:
                    result.append(fmt % (exp.address, exp.ordinal, exp.name, exp.forwarder))
            else:
                if exp.forwarder is None:
                    result.append(fmt % (exp.address, exp.ordinal, exp.name, '<Exported by ordinal>'))
                else:
                    result.append(fmt % (exp.address, exp.ordinal, exp.name, exp.forwarder))
            
        return result
        
        
    def analyzeSections(self):
        """
        This function collects the sections which are available in the PE.
        
        @rtype list
        @return list of printable lines
        """
        result = []
        result.append(self.createHeader("sections"))
        for sec in self.pe.sections:
            result.append(str(sec))
            result.append("")
        return result
    
    
    def analyzeStrings(self, minlength=5, maxlength=1000):
        result = []
        result.append(self.createHeader("Resource Strings"))
        if self.data == None:
            return result
        printable_chars = "[\x20-\x7E]{%i,%i}" % (minlength , maxlength) 
        for s in re.findall(printable_chars, self.data):
            result.append(s.strip())
        return result
    
    def analyze(self, fileInfo = True, sections = True, imports = True, 
                exports =True, res_strings=True, minlength=5, maxlength=1000):
        result = []
        if fileInfo:
            result.extend(self.fileInfo())
        if sections:
            result.extend(self.analyzeSections())
        if imports:
            result.extend(self.analyzeImports())
        if exports:
            result.extend(self.analyzeExports())
        if res_strings:
            result.extend(self.analyzeStrings(minlength, maxlength))
        print "\n".join(result)
          
            
    def createHeader(self, header):    
        result = "\n{0}\n{1}{2}{1}\n{0}"
        length = (80 - len(header)) / 2
        return result.format(LINE, "-"*length, header)
    
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-f", "--file", required=True,
                        help="file to analyse")
    parser.add_argument("--exports", required=False, action="store_true",
                        help="show exported symbols")
    parser.add_argument("--imports", required=False, action="store_true",
                        help="show exported symbols")
    parser.add_argument("--strings", required=False, action="store_true",
                        help="dump strings")
    parser.add_argument("--min", required=False, type=int , default=5, 
                        help="min. strings length")
    parser.add_argument("--max", required=False, type=int , default=1000, 
                        help="max. strings length")
    args = parser.parse_args()
    
    pa = PeAnalyzer(args.file)
    pa.analyze(exports=args.exports, imports=args.imports, 
               res_strings=args.strings, 
               minlength=args.min, maxlength=args.max)
    #pa.analyze( sections=False, imports=False)
