#!/usr/bin/env python
"""
created on 26.09.2013s

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module6/crashdump.py?at=master

This script will either load file specified by command line or attach to 
a given PID and it prints a crash dump to stdout.
"""
import argparse
from pydbg import *
from pydbg.defines import *



def accessViolationDump(dbg):
	if dbg.dbg.u.Exception.dwFirstChance:
		return DBG_EXCEPTION_NOT_HANDLED
	try:
		import utils # installed with pydbg or paimei
	except:
		print dbg.dump_context()
	else:
		crash = utils.crash_binning.crash_binning()
		crash.record_crash(dbg)
		print crash.crash_synopsis()
	
	return DBG_EXCEPTION_NOT_HANDLED

if __name__=='__main__':
	parser = argparse.ArgumentParser(description="")
	parser.add_argument("-f", "--file", required=False, default=None,
						help="File to debug")
	parser.add_argument("-p", "--pid", required=False, type=int, default=-1,
						help="PID to attach")
	args = parser.parse_args()
	file = args.file
	pid = args.pid
	
	dbg = pydbg()
	if file != None:
		dbg.load(file)
	else: 
		if pid < 0: # invalid pid
			import sys
			print "you have to specify a filename or a PID"
			sys.exit(-1)
		else:
			dbg.attach(pid)
	dbg.set_callback(EXCEPTION_ACCESS_VIOLATION, accessViolationDump)
	dbg.run()