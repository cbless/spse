"""
created on 25.09.2013

@author Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org:

https://bitbucket.org/chbb/spse/src/master/module6/disassBytes.py?at=master

"""
import argparse
import pefile
import pydasm

def disassemble(file, num_bytes, att=False):

	if att:
		format = pydasm.FORMAT_ATT
	else:
		format = pydasm.FORMAT_INTEL
	
	# the following lines of code are a modification from the pefile example
	# from https://code.google.com/p/pefile/wiki/UsageExamples 
	pe = pefile.PE(file)
	entry_point = pe.OPTIONAL_HEADER.AddressOfEntryPoint
	ibase = pe.OPTIONAL_HEADER.ImageBase
	data = pe.get_memory_mapped_image()[entry_point:entry_point + num_bytes]
	
	offset=0
	while offset < len(data):
		instruction = pydasm.get_instruction(data[offset:], pydasm.MODE_32)
		print "0x%08X"%offset, 
		print pydasm.get_instruction_string(instruction,format, entry_point + ibase + offset)
		if not instruction:
			break
		offset += instruction.length
			
if __name__=='__main__':
	parser = argparse.ArgumentParser(description="")
	parser.add_argument("-f", "--file", required=True, 
						help="File to disassemble")
	parser.add_argument("-n", required=False, type=int, default=200,
						help="Number of bytes to disassemble")
	parser.add_argument("-a", "--att", action='store_true')						
	args = parser.parse_args()
	disassemble(args.file, args.n, args.att)
	