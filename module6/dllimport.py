#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 24.09.2013

@author: Christoph (SPSE 1163)

you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module6/dllimport.py?at=master


'''
import argparse
import pefile

def printHeader(msg):
    header_str = "[+] {0} {1} {2}"
    length = (58-len(msg))/2
    printLine()
    print header_str.format("-"*length, msg, "-"*length)
    printLine()

def printLine():
    print "[+] " + "-"*60
      
def inspectFile(file, dlls):
    pe = pefile.PE(file)
    printHeader("searching for matching dlls")
    for e in pe.DIRECTORY_ENTRY_IMPORT:
        for dll in dlls:
            if (e.dll == dll):
                print "[+] Found dll: {0}".format(dll)
    printLine()
    print 
    printHeader("list of all imported dlls")
    for e in pe.DIRECTORY_ENTRY_IMPORT:
        print "[+] " +e.dll 
        for i in e.imports: 
            print "[+]\t{0}\t{1}".format(hex(i.address), i.name)
        printLine()  
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-f", "--file", required=True,  
                        help="File to inspect")
    parser.add_argument("-d", "--dll", required=False, default="",  
                        help="dll or a list of dlls to search")
    args = parser.parse_args()
    
    dlls = args.dll.split(",")
    
    inspectFile(args.file, dlls)