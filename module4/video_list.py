#!/usr/bin/env python
'''
Created on 14.05.2013

@author: christoph

https://bitbucket.org/chbb/spse/src/master/module4/video_list.py?at=master

'''
import urllib
from bs4 import BeautifulSoup

def fetchYoutube():
    """
    this function creates a list of videos from the startpage of 
    www.youtube.com 
    """
    resp = urllib.urlopen("http://www.youtube.com")
    if resp.code == 200:
        html = resp.read()
        bs = BeautifulSoup(html, "lxml")
        # find all video container
        videos = bs.find_all('div', {'data-context-item-type' : 'video'} )
        for v in videos:
            # parse the attributes (title, number of views and username) 
            title = v['data-context-item-title']
            views = v['data-context-item-views']
            user = v['data-context-item-user']
            # find all links in this container with the same title
            links = v.find_all('a', {'title': title})
            if len(links) > 0:
                # get the href attribute and build the full url
                link = 'www.youtube.com' + links[0]['href']
                # print some information about the video
                print "title: {0}, user: {1}, views:{2} , url:{3}, ".format(title, user, views, link) 


if __name__ == '__main__':    
    fetchYoutube()