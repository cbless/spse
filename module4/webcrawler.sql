SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `webcrawler` DEFAULT CHARACTER SET latin1 ;
USE `webcrawler` ;

-- -----------------------------------------------------
-- Table `webcrawler`.`sites`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `webcrawler`.`sites` (
  `idsites` INT(11) NOT NULL AUTO_INCREMENT ,
  `start_url` VARCHAR(2000) NOT NULL ,
  `url` VARCHAR(2000) NOT NULL ,
  `depth` INT(11) NOT NULL ,
  `html` LONGTEXT NOT NULL ,
  PRIMARY KEY (`idsites`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

USE `webcrawler` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
