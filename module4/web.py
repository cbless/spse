#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 26.06.2013

@author: Christoph (SPSE 1163)

This file contains some classes that are extions to the mechanize browser class.

you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/web.py?at=master


'''
import mechanize
import random



class WebBrowser(mechanize.Browser):
    """
    this class extends the mechanize Browser class and adds some functions
    to change the useragent.
    """
    def __init__(self, useragents = [], redirects=True, gzip=False, 
                 equiv=True, robots=False, referer=True):
        self.default_useragents =[
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:23.0) Gecko/20131011 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36',
            'Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0'
            ]
        mechanize.Browser.__init__(self)
        self.set_handle_redirect(redirects)
        self.set_handle_gzip(gzip)
        self.set_handle_equiv(equiv)
        self.set_handle_robots(robots) 
        self.set_handle_referer(referer)
        self.set_handle_refresh(mechanize._http.HTTPRefererProcessor(), max_time = 1)
        if len(useragents) == 0:
            useragents.extend(self.default_useragents)
        self.useragents = useragents
        cj = mechanize.CookieJar()
        self.cookie_jar = cj
        self.set_cookiejar(cj)
        self.changeUseragent()  # set a random useragent string in the HTTP Header 
    
    def printInfo(self, key, value=None):
        if value ==None:
            print "[+] {0}".format(key)
        else:
            print "[+] {0}: {1}".format(key, value)
        
    def displayCookies(self):
        self.printInfo("current cookies")
        for cookie in self.getCookies():
            self.printInfo(str(cookie))
            
    def getCookies(self):
        return self.cookie_jar
        
    def addUseragent(self, useragent):
        """
        add a new useragent to the useragent-list
        """
        self.useragents.append(useragent)
    
    def removeUseragent(self, useragent):
        """
        remove the useragent from the useragent list
        """
        self.useragents.remove(useragent)
    
    def changeUseragent(self, useragent=None):
        """
        change the useragent string for the next requests.
        
        @param useragent: useragent string which will be used, if useragent 
            is not set a random useragent will be used
        """
        if useragent == None:
            useragent = random.randint(0, len(self.useragents)-1)
        self.addheaders = [('User-Agent', useragent)]
        
    
class FormInjectionBrowser(WebBrowser):
    """
    This class is an extention for the WebBrowser class list above and the
    mechanize browser. It adds some functionality to inject all text 
    and password fields automatically. 
    """
    
    def __init__(self, injection_strings=[], expected_results=[]):
        WebBrowser.__init__(self, )
        self.injection_strings= injection_strings
        self.expected_results= expected_results
    
    
    def inject(self, url):
        self.open(url)
        forms = self.forms()
        f_nr = 0 
        for f in forms:
            self.select_form(nr=f_nr)
            self.printInfo("using form number", f_nr)
            # fill all text and password fields with some A's
            # (these fields might be required fields)
            for c in f.controls: 
                if c.type == "text" or c.type =="password":
                    self.form[c.name] = "A"*10
            # after that we try to inject every text or password field 
            # in the active form with your injection strings
            for c in f.controls:
                if c.type == "text" or c.type =="password":
                    self.printInfo("injecting field", c.name)
                    # try each injection string to the field until 
                    # we found an injection 
                    for inj in self.injection_strings:
                        self.form[c.name] = inj
                        resp = self.submit()
                        data = resp.read()
                        self.back()
                        self.select_form(nr=f_nr) # select the form again
                        injectable = False
                        for er in self.expected_results:
                            if er in data:
                                msg = "injectable (string: {0})".format(inj)
                                self.printInfo(c.name, msg)
                                injectable=True
                        if injectable == True:
                            break
            f_nr += 1