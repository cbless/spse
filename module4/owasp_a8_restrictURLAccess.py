
'''
Created on 11.09.2013

@author: Christoph (SPSE 1163)


you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/owasp_a8_restrictURLAccess.py?at=master

This tool will brute force some sectret pages on the mutillidae broken WebApp.



'''
import argparse
import urllib2


def getWordlist(filename):
    """
    Gets wordlist from file or a default wordlist.
    """
    default_wordlist = [ 'secret', 'admin', '_adm', '_admin', 'root', 
                        'administrator', 'auth', 'hidden', 'console', 'conf', 
                        '_private', 'private', 'access', 'control', 
                        'control-panel', 'phpmyadmin']
    if filename == None:
        return default_wordlist
    else:
        wordlist = []
        try: 
            f = open(filename, "r")
        except:
            print "[-] cannot open wordlist file " + filename
            print "[+] using default wordlist"
            return default_wordlist
        else:
            for line in f.readlines():
                if not line.startswith("#"):
                    wordlist.append(line.strip('\n'))
            f.close()
            return wordlist
        
def parseExtensions(ext):
    """
    Parse the extention list given as parameter 'ext'. 
    
    @param ext: list of extentions as string. Use ',' to separate the 
                extentions.  
    """
    ext_list = []
    extentions = ext.split(',')
    if len(extentions) > 0:
        for e in extentions:
            if not e.startswith("."):
                e = "." + e
            ext_list.append(e)
    return ext_list
  
    
def main(url, filename, ext):
    if not url.endswith("/"):
        url = url + "/"
    wordlist = getWordlist(filename)
    extentions = parseExtensions(ext)
    for w in wordlist:
        for e in extentions:
            page = w + e 
            req = urllib2.Request(url+page)
            req.add_header('User-agent','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0')
            try:
                resp = urllib2.urlopen(req)
                if resp.code ==200:
                    print "[+] found accessable page on: {0}".format(url+page)
                resp.close()
            except urllib2.HTTPError:
                continue
                
            
            
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This tool will brute froce some secret pages")
    parser.add_argument("-u", "--url", required=True, 
                        help="the base url")
    parser.add_argument("--wordlist", required=False, default=None, 
                        help="list with pagenames to try (one word per line)")
    parser.add_argument("--extentions", required=False, default="php,pl,jsp",
                        help="list of extentions to add to the pagenames. Use ',' to separate them.")
    args = parser.parse_args()
    
    if not args.url.startswith("http://") or args.url.startswith("https://"):
        args.url = "http://"+args.url
    main(args.url, args.wordlist, args.extentions)
    