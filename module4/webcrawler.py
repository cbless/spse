#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 21.08.2013

@author: christoph


https://bitbucket.org/chbb/spse/src/master/module4/webcrawler.py?at=master

run this tool with the option -u (url)

    python webcrawler.py -u http://www.python.org

'''

import argparse
from Queue import Queue
import threading
from urlparse import urlparse, urljoin
import urllib2
from bs4 import BeautifulSoup
import MySQLdb

class MySQLConfig:
    """ 
    this class holds the mysql connection data (host, port,
    databasename, user, password)
    """
    def __init__(self, host, port, db, user, pw):
        """
        @param host: name or ip of the MySQL database host
        @param port: port on which the MySQL database is available
        @param db: name of the MySQL database
        @param user: MySQL username
        @param pw: MySQL password 
        """
        self.host = host
        self.port = port
        self.db = db
        self.user = user
        self.pw = pw
        
    
class Crawler(threading.Thread):
    
    def __init__(self, queue, max_depth, start_url,
                 url_list, url_lock, mysql_conf):
        """
        constructor.
        
        @param queue: queue with (url, depth) tuples
        @param max_depth: maximum depth for crawling webpages
        @param start_url: start url is used to get the the domain 
        @param url_list: list with all urls
        @param url_lock: reference to a lock for url_list
        @param mysql_conf: reference to MySQLConfig object
        """
        threading.Thread.__init__(self)
        self.queue = queue
        self.max_depth = max_depth
        self.start_url = start_url
        self.start_url_netloc = self.getNetlocFromURL(start_url)
        self.url_list = url_list
        self.url_lock = url_lock
        self.mysql_conf = mysql_conf
        
    def run(self):
        while True:
            url, depth = self.queue.get()
            netloc = self.getNetlocFromURL(url)
            if netloc != self.start_url_netloc:
                print "not in same network location / domain"
                self.queue.task_done()
                continue
            try:
                req = urllib2.Request(url)
                req.add_header('User-agent','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0')
                resp = urllib2.urlopen(req)
                
                if resp.code == 200:
                    print "reading url: {0} on depth: {1}".format(url,depth)
                    html = resp.read()
                    if depth < self.max_depth:
                        links = self.findLinks(html)
                        for link in links:
                            if self.isInUrlList(link) == False:
                                self.addToUrlList(link)
                                self.queue.put((link , depth + 1))
                else:
                    print "can not open page. http error: ", resp.code
                    self.queue.task_done()
                    continue
                     
            except:
                self.queue.task_done()
                print "can not open url: ", url
                continue
            
            try:
                # connect to db and insert site
                self.insertDB(url, depth, html)
            except:
                print "can not insert url: {0} into database".format(url)
            finally:
                self.queue.task_done()
                
    def findLinks(self, html):
        """
        this function creates a list of all links which starts with the same 
        network location part 
        """
        bs = BeautifulSoup(html, "lxml")
        links = []
        for a in bs.find_all('a',href=True):
            url = urljoin(self.start_url, a['href'])
            if self.getNetlocFromURL(url) == self.start_url_netloc:
                links.append(url)
        return links
                
    def isInUrlList(self, url):
        """
        this function checks if the given url exists in the url list.
        @return True if the url exists in the url lists, False otherwise.
        """
        self.url_lock.acquire()
        inList = False
        if url in self.url_list:
            inList = True
        self.url_lock.release()
        return inList
    
    def addToUrlList(self,url):
        """
        this function appends the given url to the url list
        """
        self.url_lock.acquire()
        self.url_list.append(url)        
        self.url_lock.release()
        
            
    def getNetlocFromURL(self, url):
        """
        this function extracts the network location part
        """
        res = urlparse(url)
        return res.netloc
        
    def insertDB(self, url, depth, html):
        con = MySQLdb.connect(host=self.mysql_conf.host,
                                      port=self.mysql_conf.port,
                                      db=self.mysql_conf.db,
                                      user=self.mysql_conf.user,
                                      passwd=self.mysql_conf.pw)
        cursor = con.cursor()
        try:
            cursor.execute(""" 
                INSERT INTO sites(start_url, url, depth, html) 
                VALUES(%s, %s, %s, %s)
                """, (MySQLdb.escape_string(self.start_url), 
                      MySQLdb.escape_string(url), 
                      depth, 
                      MySQLdb.escape_string(html))
                )
            con.commit()
        except Exception as e:
            print e            
            con.rollback()
        finally:
            con.close();

          
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a simple webcrawler")
    parser.add_argument("-u", "--url", required=True, 
                        help="the base url to crawling at")
    parser.add_argument("-d", "--depth", required=False, default=5, type=int, 
                        help="use this option if you only want crawl to a particular depth")
    parser.add_argument("-t", "--threads", required=False, default=10, type=int,
                        help="number of worker threads")
    parser.add_argument("--host", required=False, default="127.0.0.1",
                        help="Host with the MySQL database")
    parser.add_argument("--user", required=False, default="webcrawler",
                        help="MySQL username")
    parser.add_argument("--pw", required=False, default="webcrawler",
                        help="MySQL password")
    parser.add_argument("--port", required=False, default=3306, 
                        help="Port on wich the MySQL database is available")
    parser.add_argument("--db", required=False, default="webcrawler",
                        help="MySQL database name")
    args = parser.parse_args()
    
    max_depth = args.depth
    start_url = args.url
    num_threads = args.threads
    
    queue = Queue()                 # create an empty queue
    url_list = []                   # list with all links 
    url_lock = threading.Lock()     # lock for url_list
    
    # store sql connection data
    mysql_conf = MySQLConfig(args.host, args.port, 
                             args.db, args.user, args.pw) 
    
    queue.put((start_url,1))        # append the start url/ depth(1) to the queue
    print "start crawling (max depth: {0}) {1}".format(max_depth,start_url)
    for i in range (num_threads):   # create some worker threads  
        worker = Crawler(queue, max_depth, start_url, 
                         url_list, url_lock, mysql_conf)
        worker.setDaemon(True)
        worker.start()
    
    queue.join()                    # block until all tasks are done
    print "finished"