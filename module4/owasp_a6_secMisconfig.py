'''
Created on 11.09.2013

@author: Christoph (SPSE 1163)


you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/owasp_a6_secMisconfig.py?at=master


this tool looks for directories listed in the robots.txt which allows a 
directory listing.

'''
import argparse
import urllib
from web import WebBrowser
from bs4 import BeautifulSoup
import re

    
def findDisallowed(robots):
    """
    Find all directories in the robots.txt which are disallowed.
    @param robots: content of the robots.txt 
    @return: list with all disallowed directories
    """
    dirs = re.findall(r"Disallow:\s(\w+\/)",robots)
    return dirs

def isDirectoryListinEnabled(url, d):
    """
    This function checks if the directory listing is enabled for the directory.
    @param url: base url
    @param d: directory
    @return: True if directorylisting is enabled, False otherwise
    """
    if not url.endswith("/"):
        url = url + "/"
    resp = urllib.urlopen(url + d)
    if resp.code == 200:
        html = resp.read()
        bs = BeautifulSoup(html, "lxml")
        t = str(bs.title)
        title_end = "</title>"
        if d.endswith("/"):
            title_end = d[0:len(d)-1] + title_end
        else:
            title_end = d + title_end
        # now tile_end looks like this: "dirname</title>"
        if t.startswith("<title>Index of") and t.endswith(title_end):
            return True
        else:
            return False
        
def getRobots(url):
    """
    Reads the robots.txt from the given URL and return its content.
    """
    if not url.endswith("/"):
        url = url + "/"
    br = WebBrowser()
    br.open(url + "robots.txt")
    robots = br.response().read()
    br.close()
    return robots
    
def main(url):
    robots = getRobots(url)
    for d in findDisallowed(robots):
        if isDirectoryListinEnabled(url, d) == True:
            print "[+] directory listing is enabled for directory: {0} ".format(d)
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this tool looks for directories which allows a directory listing")
    parser.add_argument("-u", "--url", required=True, 
                        help="url to inject")
    args = parser.parse_args()
    
    if not args.url.startswith("http://") or args.url.startswith("https://"):
        args.url = "http://"+args.url
    main(args.url)
    