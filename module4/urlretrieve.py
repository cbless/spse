#!/usr/bin/env python
'''
Created on 13.05.2013

@author: christoph

https://bitbucket.org/chbb/spse/src/master/module4/urlretrieve.py?at=master

'''
import urllib
import argparse

def report(blocknum, blocksize, totalsize):
    print "{0} percent ready".format(blocksize * blocknum * 100 / totalsize)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this tool will only download a file")
    parser.add_argument("-u", "--url", required=True, help="Url of the file")
    parser.add_argument("-f", "--file", required=True, help="destination for downloaded file")
    args = parser.parse_args()
    
    print "downloading file {0} to local path {1}".format(args.url, args.file)
    result = urllib.urlretrieve(args.url, args.file, report)
