#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on 20.06.2013

@author: christoph
'''
import urllib2
from mechanize import Browser

proxy_dict = {"user": "joe",
                     "password": "password",
                     "host": "myproxy.example.com",
                     "port": "3128"}

http_proxy = "{0}:{1}@{2}:{3}".format(proxy_dict['user'],
                                      proxy_dict['password'],
                                      proxy_dict['host'],
                                      int(proxy_dict['port']))

def add_proxy_to_urllib(proxy_string):
    # http://user:password@host:port
    proxy_str = "http://"+proxy_string
    # Proxy-Handler erstellen
    proxy_handler = urllib2.ProxyHandler({'http': proxy_str})
    opener = urllib2.build_opener(proxy_handler, urllib2.HTTPHandler)
    urllib2.install_opener(opener)          # Proxy-Handler hinzufügen
    

def mechanize_proxy_sample():
    br = Browser()
    # Note the userinfo ("joe:password@") and port number (":3128") are optional.
    br.set_proxies({"http": "joe:password@myproxy.example.com:3128",
                "ftp": "proxy.example.com",
                })
    # Add HTTP Basic/Digest auth username and password for HTTP proxy access.
    # (equivalent to using "joe:password@..." form above)
    br.add_proxy_password("joe", "password")


def add_proxy_to_beautiful_soup():
    pass