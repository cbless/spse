#!/usr/bin/env python
'''
Created on 30.03.2013

@author: christoph
'''

import argparse # needs python 2.7
import socket
from Queue import Queue, Empty
from threading import Thread
from scapy.all import sr1, IP, TCP, RandNum

class Worker (Thread):
    
    def __init__(self, ip, queue):
        Thread.__init__(self)
        self.ip = ip
        self.queue = queue
    
    
    def run(self):
        while not self.queue.empty():
            # get next port from queue
            port = self.queue.get() 
            # build an IP header with destination address
            ip=IP(dst=self.ip) 
            # TCP header with dest. port, SYN flag and a random sourceport > 1024
            tcp=TCP(dport=port, flags="S", sport=RandNum(1025,65535))
            resp = sr1(ip/tcp, verbose=False, timeout=0.5) # send and receive 1 packet
            if resp and resp[TCP].flags == 0x12:
                # (ACK = 0x10) + (SYN = 0x02) = (SYN-ACK = 0x12)
                print "port %s is open" % str(port) 
            self.queue.task_done()
            

def main():
    parser = argparse.ArgumentParser(description="this program is a \
    multi-threaded portscanner which uses SYN-Scanning")
    parser.add_argument("-p", "--port", required=False, default="1-1024", 
                        help="Port(s) to scan. Use ',' to separate ports \
                        and '-' for a range of ports")
    parser.add_argument("-t", "--target", required=True, 
                        help="Target-IP for your scan")
    args = parser.parse_args()
    portlist = parse_ports(args.port)   # create a portlist form cli parameter -p or --port
    ip = args.target  					# read target ip from cli parameter -t or --target

    q = Queue()	                     	# create an empty queue
    for p in portlist:                  # fill all ports into the queue
        q.put(p)

    for i in range (10):	            # create some threads  
        worker = Worker(ip,q)
        worker.setDaemon(True)
        worker.start()

    q.join() 							# block until all tasks are done
    
    print "finished"
        
    
def parse_ports(ports):
    """
    this function builds a portlist form the given parameter 'ports'. The 
    parameter 'ports' can contains a list of comma separated ports. Each of 
    this ports can be a portrange with a startport and an endport (separated 
    with '-'). 
    """
    portlist = []
    for port in ports.split(','):
        if '-' in port:
            portrange = port.split('-')
            for i in range(int(portrange[0]),int(portrange[1])+1):
                # append all ports in the portrange to the new portlist
                portlist.append(i)
        else:
            # append single port to portlist
            portlist.append(int(port))
    return portlist

if __name__ == '__main__':
    main()
