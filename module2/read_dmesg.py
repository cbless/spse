#!/usr/bin/env python


def read_message_log():
    """
    this function reads the file /var/log/dmesg. 
    It finds all logs in it which contains the keyword "usb" 
    and prints them out selectively
    """
    filename = "/var/log/dmesg"
    try: 
        f = open(filename, "r")
        for line in f.readlines():
            if "usb" in line.lower(): 
                print line
    except:
        print "cannot open file " + filename
    else:
        f.close()

if __name__ == '__main__':
    read_message_log()