#!/usr/bin/env python

'''
Created on 03.04.2013

@author: christoph

This program starts a TCP-Server which will automaticlly shuts down after a 
pre-configured duration. 
'''

import argparse
from SocketServer import TCPServer, BaseRequestHandler
import signal

class MyTCPServer (TCPServer):
    allow_reuse_address = True

class MyHandler(BaseRequestHandler):
    def handle(self):
        try:
            data = self.request.recv(1024).strip()
            print data                  # print received data
            self.request.sendall(data)  # return data to client
        except Exception:
            print "Error while receiving data"

def shutdown_handler(signum, frm):
    print "it is time for me to die"
    exit()
        
def ctrlc_handler(signum, frm):
    print "it is time for me to die"
    exit()
         
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="this program starts a \
    TCP-Server which automaticlly shuts down after a pre-configured \
    duration (see parameter '-s') ")
    parser.add_argument("-p", "--port", required=False, default="1337", 
                        help="port for TCP-Server")
    parser.add_argument("-s", required=True,  
                        help="duration after which the server automaticlly \
                        shuts down")
    parser.add_argument("-i", "--interface", required=False, default="127.0.0.1", 
                        help="Interface")
    args = parser.parse_args()
    
    signal.signal(signal.SIGALRM, shutdown_handler)
    signal.alarm(int(args.s))       
    signal.signal(signal.SIGINT, ctrlc_handler)
    
    server = MyTCPServer((args.interface,int(args.port)), MyHandler)
    server.serve_forever()
    
    
        
    