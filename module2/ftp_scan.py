#!/usr/bin/env python
'''
Created on 02.04.2013

@author: Christoph
'''

import argparse
import signal
from thread import allocate_lock
from threading import Thread
from Queue import Queue
from ftplib import FTP

lock = allocate_lock()

def read_ftp_list(filename):
    sites = []
    try: 
        f = open(filename, "r")
        for line in f.readlines():
            line = line.rstrip()
            if line:
                sites.append(line)
    except:
        print "cannot open file " + filename
    else:
        f.close()
    return sites


class Worker (Thread):
    
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
    
    
    def run(self):
        while not self.queue.empty():
            host = self.queue.get()
            print "connecting to "+ host
            try:
                ftp = FTP(host)             # connect to host on default port
                ftp.login()                 # login as anonymous
                result = ftp.retrlines('LIST')# list directory contents
                lock.acquire()
                print "-------------------------------"
                print "directories of host %s :\n%s" % (host, result)
                print "-------------------------------"
                lock.release()
                ftp.quit()                  # logout
            except:
                print "Connection Error"
            self.queue.task_done()
        

def sig_handler(signum , frm):
    if signum == signal.SIGINT:
        exit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-f", "--file", required=True, 
                        help="file that contains a list of ftp server")
    args = parser.parse_args()
    
    signal.signal(signal.SIGINT, sig_handler)
    
    q = Queue()                         # create an empty queue
    sites = read_ftp_list(args.file)    # read ftp server list from file
    for s in sites:                     # fill all ports into the queue
        q.put(s)

    for i in range (10):                # create some threads  
        worker = Worker(q)
        worker.setDaemon(True)
        worker.start()

    q.join()                             # block until all tasks are done
    
    print "finished"