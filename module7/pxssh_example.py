#!/usr/bin/env python
'''
Created on 16.10.2013

@author: christoph
'''

import argparse
import pxssh
import getpass

def getConnectionData(url):
    """
    this function parses the connection string and split it into two parts 
    (user and host). It also gets the password from userinput.
    
    @type url: String
    @param url: connection string (format: username@host)  
    
    @rtype: Tuple
    @return: Tuple with "user", "host" and "password"
    
    """
    if "@" not in url:
        raise Exception("Invalid URL. Use a string like: username@host")
    parts = url.split("@")
    if len (parts) != 2:
        raise Exception("Invalid URL. Use a string like: username@host")
    user , host = parts
    
    pw = getpass.getpass("password: ")
    
    return (host, user, pw)
    
    
def send_cmd(sess, cmd):
    sess.sendline(cmd)
    sess.prompt()
    print sess.before
    
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("url", metavar="URL", 
                        help="connection string (username@host)")
    args = parser.parse_args()
    
    host, user, pw = getConnectionData(args.url)
    try:
        s = pxssh.pxssh()
        s.login(host, user, pw)
    except pxssh.ExceptionPxssh, e:
        print "login failed: " + str(e)
        
    while s.isalive():
        try:
            cmd = raw_input("cmd: ")
            send_cmd(s, cmd)
        except pxssh.ExceptionPxssh, e:
            print "send_cmd failed: "
            print "closing connection."
            s.logout()
    
        
            
